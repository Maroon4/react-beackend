const mongoose = require('./libs/mongoose');
mongoose.set('debug', true);
const async = require('async');


async.series([
    open,
    dropDB,
    requireModels,
    setUsers
], function (err, results) {
    console.log(arguments);
    mongoose.disconnect();
    process.exit(err ? 255 : 0);
});

function open(callback) {
    mongoose.connection.on('open', callback);
}

function dropDB(callback) {
    const db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback) {
    require('./modeles/user').User;

    async.each(Object.keys(mongoose.models), function (modelName, callback) {
        mongoose.models[modelName].ensureIndexes(callback);
        // mongoose.models[modelName].createIndexes(callback);
    }, callback);
}

function setUsers(callback) {

    const users = [
            {username: 'Вася', password: 'supervasya'},
            {username: 'Петя', password: '123'},
            {username: 'admin', password: 'thetruehero'}
        ];

    async.each(users, function (userData, callback) {
        const user = new mongoose.models.User(userData);
        user.save(callback)
    }, callback);

    // async.parallel([
    //     function (callback) {
    //         const vasya = new User( {username: 'Вася', password: 'supervasya'},);
    //         vasya.save(function (err) {
    //             callback(err, vasya);
    //         })
    //     },
    //     function (callback) {
    //         const petya = new User( {username: 'Петя', password: '123'} );
    //         petya.save(function (err) {
    //             callback(err, petya);
    //         })
    //     },
    //     function (callback) {
    //         const admin = new User(  {username: 'admin', password: 'thetruehero'});
    //         admin.save(function (err) {
    //             callback(err, admin);
    //         })
    //     }
    // ], callback);
}

// mongoose.connection.on('open', function () {
//
//     const db = mongoose.connection.db;
//     db.dropDatabase(function (err) {
//         if (err) throw err;
//         // console.log('Ok')
//
//         async.parallel([
//             function (callback) {
//                 const vasya = new User( {username: 'Вася', password: 'supervasya'},);
//                 vasya.save(function (err) {
//                     callback(err, vasya);
//                 })
//             },
//             function (callback) {
//                 const petya = new User( {username: 'Петя', password: '123'} );
//                 petya.save(function (err) {
//                     callback(err, petya);
//                 })
//             },
//             function (callback) {
//                 const admin = new User(  {username: 'admin', password: 'thetruehero'});
//                 admin.save(function (err) {
//                     callback(err, admin);
//                 })
//             }
//         ], function (err, results) {
//              console.log(arguments);
//              mongoose.disconnect();
//         });
//
//
//
//         // const users = [
//         //     {username: 'Вася', password: 'supervasya'},
//         //     {username: 'Петя', password: '123'},
//         //     {username: 'admin', password: 'thetruehero'}
//         // ]
//
//     });
//
// });


// const  user = new User({
//     username: "Tester",
//     password: "secret"
// });
//
// user.save(function (err, user, affected) {
//    // if (err) throw err;
//    //  console.log(arguments);
//    User.findOne({username: "Tester"}, function (err, tester) {
//        console.log(tester)
//    })
// });


// const MongoClient = require('mongodb').MongoClient;
// const assert = require('assert');
//
// // Connection URL
// const url = 'mongodb://localhost:27017';
//
// // Database Name
// const dbName = 'reactbackend';
//
// // Use connect method to connect to the server
// MongoClient.connect(url, function(err, client) {
//     assert.equal(null, err);
//     console.log("Connected successfully to server");
//
//     const db = client.db(dbName);
//
//     removeDocument(db, function () {
//         console.log("Dell");
//     });
//
//     insertDocuments(db, function() {
//         client.close();
//     });
//
// });
//
// const removeDocument = function(db, callback) {
//     // Get the documents collection
//     const collection = db.collection('documents');
//     // Delete document where a is 3
//     collection.deleteOne({ a : 3 }, function(err, result) {
//         assert.equal(err, null);
//         assert.equal(1, result.result.n);
//         console.log("Removed the document with the field a equal to 3");
//         callback(result);
//     });
// };
//
// const insertDocuments = function(db, callback) {
//     // Get the documents collection
//     const collection = db.collection('documents');
//     // Insert some documents
//     collection.insertMany([
//         {a : 1}, {a : 2}, {a : 3}
//     ], function(err, result) {
//         assert.equal(err, null);
//         assert.equal(3, result.result.n);
//         assert.equal(3, result.ops.length);
//         console.log("Inserted 3 documents into the collection");
//         callback(result);
//         console.log(result);
//     });
// };