const express = require('express');
const http = require('http');
const path = require('path');
const favicon = require('static-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const conf = require('./conf');
const log = require('./libs/log')(module);
const index = require('./routes/index');
const router = require('./routes/index');
const errorHandler = require('express-error-handler');
const HttpError = require('./error').HttpError;
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
// const MongoStore = require('connect-mongo')(express);
const session = require('express-session')

const app = express();
app.set('port', conf.get('port'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jsx');
// const options = { beautify: true};
app.engine('jsx', require('express-react-views').createEngine());


app.use(favicon());
if (app.get('evn') === 'development') {
    app.use(logger('dev'));
} else {
    app.use(logger('default'));
}

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());

const MongoStore = require('connect-mongo')(session);

app.use(session({
    secret: conf.get('session:secret'),
    key: conf.get('session:key'),
    cookie: conf.get('session:cookie'),
    // useUnifiedTopology: true,
    store: new MongoStore({
        mongooseConnection: mongoose.connection,
        useUnifiedTopology: true
    }),
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

// app.use(function (req, res, next) {
//     req.session.numberOfVisits = req.session.numberOfVisits + 1 || 1;
//     res.send("Visits: " + req.session.numberOfVisits)
// });


app.use(express.static(path.join(__dirname, 'public')));
// app.use(app.router);

require('./routes')(app);






app.use(express.static(path.join(__dirname, 'public')));

app.use(function (err, req, res, next) {

    if(typeof  err == 'number') {
        err = new HttpError(err);
    }

    if(err instanceof HttpError) {
        res.sendHttpError(err);
    } else {
        if (app.get('env') == 'development') {
            errorHandler()(err, req, res, next);
        } else {
            log.error(err);
            err = new HttpError(500);
            res.sendHttpError(err);
        }
    }

    // if (app.get('env') === 'development') {
    //     app.use(function(err, req, res, next) {
    //         res.status(err.status || 500);
    //         res.render('error', {
    //             message: err.message,
    //             error: err
    //         });
    //     });
    // }
    // else {
    //     res.send(500);
    // }

});

// // catch 404 and forwarding to error handler
// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });
//
// // error handlers
//
// // development error handler
// // will print stacktrace
// if (app.get('env') === 'development') {
//     app.use(function(err, req, res, next) {
//         res.status(err.status || 500);
//         res.render('error', {
//             message: err.message,
//             error: err
//         });
//     });
// }
//
// // production error handler
// // no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: {}
//     });
// });




http.createServer(app).listen(conf.get('port'), function () {
    log.info('Express server listening on port' + conf.get('port'))
});



module.exports = app;
