const User = require('../modeles/user').User;
const HttpError = require('../error');
const ObjectID = require('mongodb').ObjectID;

module.exports = function(app) {
  app.get('/', require('./frontpage').get);

  app.get('/signin', require('./formLogin').get);
  app.post('/signin', require('./formLogin').get);

  app.get('/chat', require('./chat').get);


  app.get('/users', function (req, res, next) {
    User.find({}, function (err, users) {
      if (err) return next(err);
      res.json(users);
    })
  });

  app.get('/users/:id', function (req, res, next) {
    try {
      var id = new ObjectID(req.params.id);
    } catch (e) {
      next(404);
      return;
    }

    User.findById(id, function (err, user) {
      if (err) return next(err);
      if (!user) {
        next(new  HttpError(404, "User not found"));
      }
      res.json(user);
    });
  });
};

// exports.index = function(req, res){
//   res.render('index', {
//     name: 'Max',
//     title: 'General21313'
//   });
// };

