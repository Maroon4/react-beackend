const React = require('react');
const DefaultLayout = require('./layout/default');


function HelloMessage(props) {
    return (
        <DefaultLayout title={props.title}>
            <div>Hello {props.name}</div>
        </DefaultLayout>

    );
}

module.exports = HelloMessage;