// import React, {Component} from "react";
// import {Link} from 'react-router-dom'

const React = require('react');



import './header.css'



class Header extends React.Component{

    render() {
        return (

            <html>
            <div className = "row header d-flex" >
                <div className="col-1 d-flex">
                    <h3>
                        <Link to="/">
                            React Backend
                        </Link>
                    </h3>
                </div>
                {/*<div className="segment col-1 d-flex">*/}
                <ul className="segment col-1 d-flex header">
                    <li className="d-flex">
                        <a href="#" >Чат</a>
                    </li>
                </ul>
                {/*</div>*/}
                <div className="col-auto d-flex">
                    <form
                        className="button-panel d-flex">

                        <input type="text"
                               className="form-control search-input d-flex"
                               placeholder="type to search"
                            // value={this.state.term}
                            // onChange={ this.onTermChange }
                        />
                        <button className='button-search btn btn-success btn-lg'>
                            Пошук
                        </button>
                    </form>
                </div>
                <div className="col-2 d-flex">
                    <button className="row button-cabinet btn-lg d-flex">
                        <Link to="/autorisepage/">
                            Особистий кабінет
                        </Link>
                    </button>
                </div>


            </div>
            </html>

        );
    }

}

module.exports = Header;