const React = require('react');



function FormLogin() {

    return (
        <html>
        <head>
            <title>Formlogin</title>
            <div>Сторінка авторизаціх</div>
        </head>
        <body>
        <div>
            <form className="form-horizontal login-form" name="login-form">
                <div className="form-group">
                    <label htmlFor="input-username" className="col-lg-2 control-label">Имя</label>

                    <div className="col-lg-10">
                        <input name="username" value="name" type="text" className="form-control" id="input-username"
                               placeholder="Имя"/>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="input-password" className="col-lg-2 control-label">Пароль</label>

                    <div className="col-lg-10">
                        <input name="password" value="pass" type="password" className="form-control" id="input-password"
                               placeholder="Пароль"/>
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-lg-offset-2 col-lg-10">
                        <button type="submit" className="btn btn-primary" data-loading-text="Отправляю...">Войти
                        </button>
                        <span className="help-block error"></span>
                    </div>
                </div>
            </form>

        </div>
        </body>
        </html>
    )

}

module.exports = FormLogin;


